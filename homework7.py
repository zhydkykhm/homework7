persons = [
    {"name": "John", "age": 15},
    {"name": "Bob", "age": 15},
    {"name": "Max", "age": 32},
    {"name": "Jack", "age": 45}
]

############################## Задание_1.а
# а) Напечатать имя самого молодого человека.
# Если возраст совпадает - напечатать все имена самых молодых.

age_min = persons[0]["age"]
names_min = []
for dictionary in persons:
    if dictionary["age"] == age_min:
        names_min.append(dictionary["name"])
print(age_min, names_min)

############################## Задание_1.б
# б) Напечатать самое длинное имя.
# Если длина имени совпадает - напечатать все имена.

max_len = float('-inf')
for person in persons:
    if len(person['name']) > max_len:
        max_len = len(person['name'])
names_max_len = [p['name'] for p in persons if len(p['name']) == max_len]
print(names_max_len)

############################## Задание_1.в
# в) Посчитать среднее количество лет всех людей из списка.

ages = []
for p in persons:
    age = p['age']
    ages.append(age)
print(sum(ages) // len(ages))

##############################

my_dict_1 = {i: str(i) for i in range(5) if i > 0}
my_dict_2 = {i: str(i) for i in range(5) if i > 2}

############################## Задание_2.а
# а) Создать список из ключей, которые есть в обоих словарях.

print(list((key, my_dict_1[key]) for key in my_dict_1 if key in my_dict_2))

############################## Задание_2.б
# б) Создать список из ключей, которые есть в первом, но нет во втором словаре.

print(list((key, my_dict_1[key]) for key in my_dict_1 if key not in my_dict_2))

############################## Задание_2.в
# в) Создать новый словарь из пар {ключ:значение},
# для ключей, которые есть в первом, но нет во втором словаре.

result_dict = {key: my_dict_1[key]  for key in set(my_dict_1) - set(my_dict_2)}
print(result_dict)

############################## Задание_2.г
# г) Объединить эти два словаря в новый словарь по правилу:
# если ключ есть только в одном из двух словарей - поместить пару ключ:значение,
# если ключ есть в двух словарях - поместить пару
# {ключ: [значение_из_первого_словаря, значение_из_второго_словаря]},

my_dict_4 = {}
for x in my_dict_1.keys() | my_dict_2.keys():
    if (x in my_dict_1):
        if (x in my_dict_2):
            my_dict_4[x] = [my_dict_1[x], my_dict_2[x]]
        else:
            my_dict_4[x] = my_dict_1[x]
    else:
        my_dict_4[x] = my_dict_2[x]
print(my_dict_4)
